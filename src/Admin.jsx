
import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import Ranking from './Ranking';

const Admin = (props) => {

    return (
        <>
        <br/>
        <br/>
            <Link className="btn btn-primary btn-sm" to="/admin/nuevo">
                CREAR NUEVO ROBOT
            </Link>
            <br/>
            <br/>
            <Ranking/>           
        </>
    )
};

export default Admin;

