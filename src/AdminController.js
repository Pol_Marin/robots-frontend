const api_url = 'http://localhost:8080/api/robots';
const api_batalla = 'http://localhost:8080/api/batalla';

export default class Controller {

    //Devuelve todos los robots
     static getAll = async () => {
       let resp = await fetch(api_url);
       if (!resp.ok){
           throw new Error('Error en fetch');
       } else {
           resp = await resp.json();
           return resp.map(el => ({...el, id: el._id}));                 
       }
    }

    //Añade un robot
    static addRobot = (item) => {
        const jsonContacte = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonContacte,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url, opcionesFetch)
            .then(resp => {
                console.log("nuevo contacto:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    static combate = (item1,item2) => {
        console.log(item2);
        fetch(api_batalla+"/"+item1._id+"/"+item2._id)
            .then(resp => {
                console.log("Ganador:", resp.nombre)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }
}