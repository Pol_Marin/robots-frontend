
import React, { useState } from "react";
import { Container, FormGroup, Label, Input, Button } from 'reactstrap';
import {Redirect} from "react-router-dom";

import Controller from './AdminController';

const Nuevo = (props) => {

    const [nom, setNom] = useState('');
    const [ataque, setAtaque] = useState(0);
    const [defensa, setDefensa] = useState(0);
    const [vida, setVida] = useState(0);

    const [volver, setVolver] = useState();

    const guardar = () => {
        const robotNuevo = {
            nombre: nom,
            ataque,
            defensa,
            vida,
        };

        Controller.addRobot(robotNuevo);
        setVolver(true);
    }


  if (volver){
    return <Redirect to="/admin" />
  }


    return (
        <>
            <Container>
            <h3 className="mt-4">CREA UN NUEVO ROBOT</h3>
                <FormGroup>
                    <Label for="nom">Nombre</Label>
                    <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
                </FormGroup>

                <FormGroup>
                    <Label for="ataque">ataque</Label>
                    <Input type="number" name="ataque" id="ataque" value={ataque} onChange={(e) => setAtaque(e.target.value)} />
                </FormGroup>

                <FormGroup>
                    <Label for="defensa">defensa</Label>
                    <Input type="text" name="defensa" id="defensa" value={defensa} onChange={(e) => setDefensa(e.target.value)} />
                </FormGroup>

                <FormGroup>
                    <Label for="vida">vida</Label>
                    <Input type="text" name="vida" id="vida" value={vida} onChange={(e) => setVida(e.target.value)} />
                </FormGroup>

                <Button className='btn btn-success' onClick={guardar} >Guardar</Button>
            </Container>
        </>
    );
};


export default Nuevo;