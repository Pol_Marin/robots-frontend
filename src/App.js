import logo from './logo.svg';
import './App.css';
import { BrowserRouter, NavLink, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Play from './Play';
import Admin from './Admin';
import Nuevo from './Nuevo';

function App() {
  return (
    <BrowserRouter>
      <ul className="nav nav-tabs">
        <li className="nav-item">
          <NavLink exact className="nav-link" to="/play">
            Play
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/admin">
            Administracion
          </NavLink>
        </li>
      </ul>
      <Switch>
        <Route exact path="/play" component={Play} />
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/admin/nuevo" component={Nuevo} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
