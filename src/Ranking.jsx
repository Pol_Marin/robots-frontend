import React, { useState, useEffect } from "react";
import { Table } from 'reactstrap';
import Controller from './AdminController';

const Ranking = (props) => {

    const [dades, setDades] = useState([]);
    const [error, setError] = useState('');

    useEffect(()=>{
        Controller.getAll()
         .then(robots =>{
            return robots.map((el) => {
                return <tr>
                            <td>{el.nombre}</td>
                            <td>{el.caracteristicas.ataque}</td>
                            <td>{el.caracteristicas.defensa}</td>
                            <td>{el.caracteristicas.vida}</td>
                            <td>{el.experiencia.victorias}</td>
                            <td>{el.experiencia.jugadas}</td>
                        </tr>
            });
         })
         .then(data => setDades(data))
         .catch(err => setError(err.message));
    }, [])

    return (
        <>
            <Table dark>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ataque</th>
                        <th>Defensa</th>
                        <th>Vida</th>
                        <th>Victorias</th>
                        <th>Jugadas</th>
                    </tr>
                </thead>
                <tbody>
                    {dades}
                </tbody>
            </Table>
        </>
    )
};

export default Ranking;