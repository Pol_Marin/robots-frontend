import React, { useState, useEffect } from 'react';
import Controller from './AdminController';

export default () => {
    const [mensaje, setMensaje] = useState("Elige el primer robot");
    const [robot1,setRobot1] = useState([]);
    const [robot2,setRobot2] = useState([]);
    const [ganador, setGanador] = useState("");
    const [robots, setRobots] = useState("");
    const [error, setError] = useState("");

    // HACER GET DE TODOS LOS ROBOTS

    useEffect(()=>{
        Controller.getAll()
         .then(robots =>{
            return robots.map((el) => {
                return <tr>
                            <td>{el.nombre}</td>
                            <td>{el.caracteristicas.ataque}</td>
                            <td>{el.caracteristicas.defensa}</td>
                            <td>{el.caracteristicas.vida}</td>
                            <td>{el.experiencia.victorias}</td>
                            <td>{el.experiencia.jugadas}</td>
                            <td><button className="btn btn-light" onClick={ ()=>elegir(el)}>Elegir</button></td>
                        </tr>
            });
         })
         .then(data => setRobots(data))
         .catch(err => setError(err.message));
    }, [])

    /* useEffect(() =>{
        Controller.combate(robot1, robot2)
        .then (dades => console.log(dades))
        .catch (err => console.log(err))
    }, [robot2]) */

    const elegir = (el) => {
        if (robot1){
            setRobot1(el.nombre);
            console.log("arriba");
            setMensaje("Elige el segundo robot");
        } else {
            console.log("abajo");
            setRobot2(el);

            /* 
            Controller.combate(robot1, robot2)
             .then (dades => console.log(dades))
             .catch (err => console.log(err)) */
            // HACER FETCH Y VACIAR SETROBOTS??
            // SET GANADOR CON LA RESPUESTA DEL FETCH
        }
    }

    return (
        <>
            <br></br>
            <h1>{mensaje}</h1>
            <br></br>
            <table className="table table-dark">
                <tr>
                    <th>Nombre</th>
                    <th>Ataque</th>
                    <th>Defensa</th>
                    <th>Vida</th>
                    <th>Victorias</th>
                    <th>Partidas</th>
                </tr>
                {robots}
            </table>
        </>
    )
}